const app = {
    getHomepage(req, res){
        return res.render("homepage");
    },
    getRegister(req, res){
        return res.render("register", {message: null});
    },
    getLogin(req, res){
        return res.render("login", {message: null});
    }
}

module.exports = app;