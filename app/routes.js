const router = require('express').Router();
const app = require('./controllers/appControllers');

router.get("/", app.getHomepage);
router.get("/register", app.getRegister);
router.get("/login", app.getLogin);

module.exports = router;