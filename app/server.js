if(process.env.NODE_ENV !== 'production'){
    require('dotenv').config()
}

const express = require('express');
const cors = require('cors');
const favicon = require('serve-favicon');
const morgan  = require('morgan');
const session = require('express-session');
const path = require('path');
const routes = require('./routes');

const port = process.env.PORT || 5000;
const app = express();

app.set('view engine', 'ejs')
app.set('trust proxy', 1);

app.use(favicon(path.join(__dirname, 'public', '/assets/logo-k3.ico')));
app.use(morgan('tiny'));
app.use(express.json());
app.use(express.urlencoded({extended:false}));
app.use(cors());
app.use(
    session({
        secret: process.env.SESSION_KEY,
        name: "uniqueSessionID",
        resave: false,
        saveUninitialized: false,
    })
);
// Set public dir to be static
app.use("/public", express.static('public'));
app.use(routes); // set app routes from routes file



app.listen(port, () => console.log(`Server listening at http://localhost:${port}`));
